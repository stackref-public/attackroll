<div align="center">

# Attack Roll

### Narrative-focused D&amp;D Dungeon Master

Powered by <a href="https://openai.com">OpenAI</a>&trade;

<img alt="Attack Roll" src="s3_content/images/attack-roll-logo.png" />

</div>

Using OpenAI's custom GPTs, **Attack Roll** performs as a D&amp;D dungeon master that you can use alone or with a group.

**Features:**

- Creates a character sheet for each player, including a black &amp; white image of each character.
- Generates a map of a player's location when asked.
- Utilizes a dice rolling API for applicable dice rolls!
- References official D&amp;D player, dungeon master, and monster manuals to accurately track abilities and stats.

Check it out at https://attackroll.com
