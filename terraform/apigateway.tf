resource "aws_apigatewayv2_api" "attackroll" {
  name          = "attackRoll"
  description   = "Handler for client requests"
  protocol_type = "HTTP"
  version       = "1.0.0"

  disable_execute_api_endpoint = false

  cors_configuration {
    allow_credentials = false
    allow_headers     = ["*"]
    allow_methods = [
      "GET",
      "HEAD",
      "OPTIONS",
    ]
    allow_origins = ["*"]
  }
}

resource "aws_apigatewayv2_stage" "attackroll_default" {
  api_id      = aws_apigatewayv2_api.attackroll.id
  name        = "$default"
  description = "Default stage"
  auto_deploy = true

  access_log_settings {
    destination_arn = aws_cloudwatch_log_group.api_gateway.arn
    format          = "$context.identity.sourceIp - - [$context.requestTime] \"$context.httpMethod $context.routeKey $context.protocol\" $context.status $context.responseLength $context.requestId"
  }

  default_route_settings {
    throttling_burst_limit = 100
    throttling_rate_limit  = 2000
  }
}

resource "aws_apigatewayv2_integration" "attackroll_dice" {
  api_id           = aws_apigatewayv2_api.attackroll.id
  integration_type = "AWS_PROXY"

  connection_type        = "INTERNET"
  description            = "attackRollDice"
  integration_method     = "GET"
  integration_uri        = aws_lambda_function.attackroll_dice.arn
  payload_format_version = "2.0"
  timeout_milliseconds   = 30000
}

resource "aws_apigatewayv2_route" "attackroll_dice" {
  api_id    = aws_apigatewayv2_api.attackroll.id
  route_key = "GET /api/diceRoll"
  target    = "integrations/${aws_apigatewayv2_integration.attackroll_dice.id}"
}
