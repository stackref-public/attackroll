exports.main = async (event) => {
    try {
      // Extract query string parameters
      const queryParams = event.queryStringParameters;
      const nbde = parseInt(queryParams.nbde, 10); // Number of dice
      const tpde = parseInt(queryParams.tpde, 10); // Type of dice (sides)
      const maxNbde = 50; // Max number of dice to permit
      const maxTpde = 100; // Max number of die sides to permit
    
      // Validate input
      if (isNaN(nbde) || isNaN(tpde) || nbde <= 0 || nbde > maxNbde || tpde < 2 || tpde > maxTpde || tpde % 2 !== 0) {
          return {
              statusCode: 400,
              headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*",
              },
              body: JSON.stringify({ error: "Invalid 'nbde' or 'tpde' query parameter values." }),
          };
      }
    
      // Function to roll a single die
      const rollDie = (sides) => Math.floor(Math.random() * sides) + 1;
    
      // Generate dice rolls
      const results = [];
      let total = 0;
      for (let i = 0; i < nbde; i++) {
          const roll = rollDie(tpde);
          results.push({
              id: i + 1,
              value: roll
          });
          total += roll;
      }
    
      // Return response with results and total
      return {
          statusCode: 200,
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify({ results, total }), // Include the total in the response
      };
    } catch (error) {
          return {
              statusCode: 500,
              headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*",
              },
              body: JSON.stringify({ error: "An unknown error occurred." }),
          };
    }
};
