resource "aws_route53_zone" "attackroll_com" {
  name = "attackroll.com"
}

resource "aws_route53_record" "attackroll_com_txt" {
  zone_id = aws_route53_zone.attackroll_com.zone_id
  name    = ""
  type    = "TXT"
  ttl     = 36400
  records = var.route53_txt_records
}

resource "aws_route53_record" "attackroll_com" {
  zone_id = aws_route53_zone.attackroll_com.zone_id
  name    = "attackroll.com"
  type    = "A"
  alias {
    name                   = aws_cloudfront_distribution.attackroll_com.domain_name
    zone_id                = aws_cloudfront_distribution.attackroll_com.hosted_zone_id
    evaluate_target_health = false
  }
}
