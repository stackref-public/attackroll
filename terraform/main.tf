provider "aws" {
  region = "us-east-1"

  default_tags {
    tags = {
      terraform_managed = "true"
    }
  }
}

terraform {
  required_version = ">= 1.6"
}

data "aws_caller_identity" "current" {}
data "aws_region" "current" {}
