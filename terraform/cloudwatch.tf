resource "aws_cloudwatch_log_group" "api_gateway" {
  name              = "apigateway/attackRoll"
  retention_in_days = 30
}
