resource "aws_cloudfront_origin_access_identity" "attackroll_com" {
  comment = "attackroll.com"
}

data "aws_cloudfront_origin_request_policy" "AllViewer" {
  name = "Managed-AllViewer"
}

data "aws_cloudfront_origin_request_policy" "AllViewerExceptHostHeader" {
  name = "Managed-AllViewerExceptHostHeader"
}

data "aws_cloudfront_response_headers_policy" "CORS-with-preflight-and-SecurityHeadersPolicy" {
  name = "Managed-CORS-with-preflight-and-SecurityHeadersPolicy"
}

data "aws_cloudfront_cache_policy" "CachingOptimized" {
  name = "Managed-CachingOptimized"
}

data "aws_cloudfront_cache_policy" "CachingDisabled" {
  name = "Managed-CachingDisabled"
}

resource "aws_cloudfront_origin_access_control" "attackroll_com" {
  name                              = "attackroll.com"
  description                       = "attackroll.com access control"
  origin_access_control_origin_type = "s3"
  signing_behavior                  = "always"
  signing_protocol                  = "sigv4"
}

resource "aws_cloudfront_distribution" "attackroll_com" {
  enabled             = true
  is_ipv6_enabled     = true
  comment             = "attackroll.com"
  default_root_object = "index.html"
  aliases             = ["attackroll.com"]

  origin {
    domain_name              = aws_s3_bucket.attackroll_com.bucket_regional_domain_name
    origin_access_control_id = aws_cloudfront_origin_access_control.attackroll_com.id
    origin_id                = "attackRollS3Bucket"
  }

  # API
  origin {
    connection_attempts = 3
    connection_timeout  = 10
    domain_name         = replace(aws_apigatewayv2_api.attackroll.api_endpoint, "https://", "")
    origin_id           = "attackRollAPI"

    custom_origin_config {
      http_port                = 80
      https_port               = 443
      origin_keepalive_timeout = 10
      origin_protocol_policy   = "https-only"
      origin_read_timeout      = 30
      origin_ssl_protocols = [
        "TLSv1.2",
      ]
    }
  }

  default_cache_behavior {
    allowed_methods = [
      "HEAD",
      "GET",
      "OPTIONS",
    ]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "attackRollS3Bucket"

    cache_policy_id            = data.aws_cloudfront_cache_policy.CachingOptimized.id
    response_headers_policy_id = data.aws_cloudfront_response_headers_policy.CORS-with-preflight-and-SecurityHeadersPolicy.id

    compress               = true
    viewer_protocol_policy = "https-only"
  }

  ordered_cache_behavior {
    allowed_methods = [
      "GET",
      "HEAD",
      "OPTIONS",
    ]
    cached_methods = ["GET", "HEAD"]

    cache_policy_id            = data.aws_cloudfront_cache_policy.CachingDisabled.id
    response_headers_policy_id = data.aws_cloudfront_response_headers_policy.CORS-with-preflight-and-SecurityHeadersPolicy.id
    origin_request_policy_id   = data.aws_cloudfront_origin_request_policy.AllViewerExceptHostHeader.id

    compress               = false
    path_pattern           = "/api/*"
    target_origin_id       = "attackRollAPI"
    viewer_protocol_policy = "https-only"
  }

  price_class = "PriceClass_100"

  restrictions {
    geo_restriction {
      restriction_type = "none"
      locations        = []
    }
  }

  custom_error_response {
    error_code         = 404
    response_code      = 200
    response_page_path = "/index.html"
  }

  tags = {
    Name = "attackroll.com"
  }

  viewer_certificate {
    acm_certificate_arn      = data.aws_acm_certificate.attackroll_com.arn
    minimum_protocol_version = "TLSv1.2_2021"
    ssl_support_method       = "sni-only"
  }
}
