resource "aws_s3_bucket" "attackroll_com" {
  bucket = "attackroll.com"

  tags = {
    Name = "attackroll.com"
  }
}

resource "aws_s3_bucket_website_configuration" "attackroll_com" {
  bucket = aws_s3_bucket.attackroll_com.id

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "index.html"
  }

}

resource "aws_s3_bucket_public_access_block" "attackroll_com" {
  bucket = aws_s3_bucket.attackroll_com.id

  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}
