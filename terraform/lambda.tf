data "archive_file" "attackroll_dice_lambda_payload" {
  type       = "zip"
  source_dir = "./lambda_src"
  excludes = [
    "node_modules"
  ]
  output_path = "./lambda_payload.zip"
}

resource "aws_lambda_function" "attackroll_dice" {
  filename         = data.archive_file.attackroll_dice_lambda_payload.output_path
  function_name    = "attackRollDice"
  description      = "Creates Assets in S3 and references in DB"
  role             = aws_iam_role.attackroll_dice_lambda.arn
  handler          = "main.main"
  source_code_hash = data.archive_file.attackroll_dice_lambda_payload.output_base64sha256
  runtime          = "nodejs20.x"
  timeout          = 10
  publish          = "true"
  architectures    = ["arm64"]

  tracing_config {
    mode = "Active"
  }
}

resource "aws_lambda_permission" "attackroll_dice" {
  statement_id  = "AllowAPIGatewayInvoke-attackRollDice"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.attackroll_dice.arn
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_apigatewayv2_api.attackroll.execution_arn}/*/*/attackRollDice"
}

